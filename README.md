SparkLoader

1. library to load files to spark as block sized partitions.
2. small files can be combind and loaded to avoid overhead with many mappers.
3. efficiently span big files to multiple mappers with each partition has same amount of data to process.